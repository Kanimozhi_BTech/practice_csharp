﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sum_array
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] a = { 2, 5, 7, 8, 9 };
            int n = a.Length;
            int i, sum = 0;
  
            for (i = 0; i < n; i++)
            {
                sum += a[i];
            }
            Console.WriteLine(sum);
            Console.ReadKey();
        }
       
    }
}
