﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace no_guess
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int count = 10,i=1;
            int guess_no = 15;
            while (i <= count)
            {
                int user_input=Convert.ToInt32(Console.ReadLine());
                int cnt = count - i;
                if (user_input > guess_no)
                {
                    Console.WriteLine("You have entered more than the guessed no. and you have "+ cnt+ " chances left");
                }
                else if (user_input < guess_no)
                {
                    Console.WriteLine("You have entered less than the guessed no. and you have " +cnt + " chances left");
                }
                else
                {
                    Console.WriteLine("You have guessed the correct no.");
                    break;
                }
                count--;
            }
            if (count > 10) {
                Console.WriteLine("You have losed all the chances and have not guessed the correct no.");
           
            }
            Console.ReadKey();
        }
    }
}
