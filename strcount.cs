﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace strcount
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string str;
            int alp, digit, spclch, i, j;
            alp = digit = spclch = i = 0;
            Console.Write("Input the string : ");
            str = Console.ReadLine();
            j = str.Length;

            while (i < j)
            {
                if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z'))
                {
                    alp++;
                }
                else if (str[i] >= '0' && str[i] <= '9')
                {
                    digit++;
                }
                else
                {
                    spclch++;
                }

                i++;
            }

            Console.Write("No. of Alphabets in the string is : {0}\n", alp);
            Console.Write("No. of Digits in the string is : {0}\n", digit);
            Console.Write("No. of Special characters in the string is : {0}\n\n", spclch);
            Console.ReadKey();
        }
    
    }
}
